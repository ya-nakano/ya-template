<svg width="44px" height="44px" viewBox="0 0 44 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 40.1 (33804) - http://www.bohemiancoding.com/sketch -->
    <title>menu01</title>
    <desc>Created with Sketch.</desc>
    <defs>
        <rect id="path-1" x="0" y="0" width="44" height="44"></rect>
    </defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Artboard" transform="translate(-720.000000, -39.000000)">
            <g id="menu01" transform="translate(720.000000, 39.000000)">
                <mask id="mask-2" fill="white">
                    <use xlink:href="#path-1"></use>
                </mask>
                <use id="Mask" fill="#FFFFFF" xlink:href="#path-1"></use>
                <rect class="menuicon" id="Rectangle" fill="#000000" mask="url(#mask-2)" x="0" y="0" width="44" height="4" rx="2"></rect>
                <rect class="menuicon" id="Rectangle-Copy" fill="#000000" mask="url(#mask-2)" x="0" y="10" width="44" height="4" rx="2"></rect>
                <rect class="menuicon" id="Rectangle-Copy-2" fill="#000000" mask="url(#mask-2)" x="0" y="20" width="44" height="4" rx="2"></rect>
                <text class="menuicon" id="MENU" mask="url(#mask-2)" font-family="OdudoSoft-Bold, Odudo Soft" font-size="17.2000008" font-weight="bold" fill="#000000">
                    <tspan x="-0.5" y="43.8000031">MENU</tspan>
                </text>
            </g>
        </g>
    </g>
</svg>