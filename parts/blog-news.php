<h2>新着情報</h2>
<section id="news">
	<?php query_posts("post_type=post&posts_per_page=5"); //表示する記事をクエリします。 ?>
	<?php if(have_posts()): ?>
		<ul>
			<?php while(have_posts()):  the_post();  ?>
				<?php // ここで取得したカテゴリー名、スラッグを格納します。
				$cat = get_the_category();
				$catname = $cat[0]->cat_name;
				$catslug = $cat[0]->slug;
				?>
				<li><span class="<?php echo $catslug; ?>"><?php echo $catname; ?></span> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><span class="ymd">（<?php the_time('Y年m月d日'); ?>）</span></li>
			<?php endwhile; ?>
		</ul>
	<?php else: ?>
		<p>現在投稿記事はありません。</p>
	<?php endif; ?>
	<?php wp_reset_query(); // 複数の記事表示を実現するため、クエリをリセットします。 ?>
</section>