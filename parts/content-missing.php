<div id="post-not-found" class="hentry">
	
	<?php if ( is_search() ) : ?>
		
		<header class="article-header">
			<h1><?php _e( '申し訳ございません。内容が見つかりませんでした。', '' );?></h1>
		</header>
		
		<section class="entry-content">
			<p><?php _e( 'もう一度検索をお試しください。', '' );?></p>
		</section>
		
		<section class="search">
		    <p><?php get_search_form(); ?></p>
		</section> <!-- end search section -->
		
		<footer class="article-footer">
			<p><?php _e( 'これはエラーメッセージです。「parts/content-missing.php template」', '' ); ?></p>
		</footer>
		
	<?php else: ?>
	
		<header class="article-header">
			<h1><?php _e( '投稿は見つかりませんでした。', '' ); ?></h1>
		</header>
		
		<section class="entry-content">
			<p><?php _e( '目的の内容が見つかりませんでした。(E-003)', '' ); ?></p>
		</section>
		
		<section class="search">
		    <p><?php get_search_form(); ?></p>
		</section> <!-- end search section -->
		
		<footer class="article-footer">
		  <p><?php _e( 'これはエラーメッセージです。「parts/missing-content.php template」', '' ); ?></p>
		</footer>
			
	<?php endif; ?>
	
</div>
