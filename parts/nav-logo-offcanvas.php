<!--
#### ロゴの表示 ####
align-center, align-left, align-right　で横揃え
-->
<div class="row align-justify align-middle">
    <div class="columns small-expand">
        <a href="<?php echo home_url(); ?>">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="ロゴ画像">
        </a>
    </div>
    <div class="columns shrink humberger-menu">
        <a data-toggle="off-canvas">
            <?php get_template_part( 'parts/svg/menu','00' ); ?>
            <!--?php _e( 'Menu', 'jointswp' ); ?-->
        </a>
    </div>
</div>



