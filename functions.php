<?php
// Theme support options
require_once(get_template_directory() . '/assets/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory() . '/assets/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory() . '/assets/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory() . '/assets/functions/menu.php');

// Register sidebars/widget areas
require_once(get_template_directory() . '/assets/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory() . '/assets/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory() . '/assets/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory() . '/assets/translation/translation.php');

// Remove 4.2 Emoji Support
// require_once(get_template_directory().'/assets/functions/disable-emoji.php'); 

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php'); 

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php');

/****** ここからオリジナルPHP関数 *****/

// 固定ページだけ<br><p>の自動整形機能をオフ
require_once(get_template_directory().'/assets/functions/ya/no-styling.php');

// 3xまでの画像を自動で読み込む関数
require_once(get_template_directory().'/assets/functions/ya/retina-image.php');

// 管理画面用ショートコードの追加
require_once(get_template_directory().'/assets/functions/ya/page-short-code.php');

// カスタムCSS機能
require_once(get_template_directory().'/assets/functions/ya/custom-css.php');

// 管理画面からSVGを投稿できる機能を追加
require_once(get_template_directory().'/assets/functions/ya/post-svg.php');

// アーカイブページでのタイトルに「カテゴリ：」「日付：」が表示されるのを削除
require_once(get_template_directory().'/assets/functions/ya/archive-title-fix.php');

// ACFのオプションページ機能を追加
//require_once(get_template_directory().'/assets/functions/ya/acf-option-page.php');



/****** ここまでオリジナルPHP関数 *****/

/****** ここから内容を変更しうるPHP関数 *****/

/********************
 * // WP純正のjQueryの仕様を無効化
 * // 自作の.jsを読み込む場合は、こちらで読み込むのが得策です。
 * // 2016年 夏頃追加
 ********************/
function load_cdn()
{
    if (!is_admin()) {
        wp_deregister_script('jquery');
//        レガシー対応のjQueryです。3.0.0が動かない場合はこちらを使用してください。
        wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js', array(), '1.12.3');
//        最新版のjQueryです。軽量化されているので、可能であればこちらを使用してください。
//        wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js', array(), '3.0.0');
        // 画面内に表示されたらアニメーションをする「inview」を読み込みます。
        wp_enqueue_script('inview', get_template_directory_uri() . '/assets/js/jquery.inview.js');
        // 「inview」を使用したアニメーションスニペットです。
        wp_enqueue_script('anime', get_template_directory_uri() . '/assets/js/animation.js');
    }
}

add_action('init', 'load_cdn');


