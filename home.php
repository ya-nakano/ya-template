<?php get_header(); ?>

<div id="content">
	
	<div id="inner-content" class="row">

		<main id="main" class="columns" role="main">

			<!-- 新着ブログ -->
			<setcion class="fadeIn">
				<div class="row anime">
					<div class="columns">
						<?php get_template_part('parts/blog','news'); ?>
					</div>
				</div>
			</setcion>

			<?php wp_img3x('logo','.png','テスト'); ?>

		</main> <!-- end #main -->

		<?php get_sidebar(); ?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>