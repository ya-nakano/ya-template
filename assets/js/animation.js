// *** 使用方法 ***
//
// <div class="flide2Left">
//  <div class="anime">
//    .animeを含む要素がアニメーションされます。
//    .animeをつけなければアニメーションしないため、混ぜて使用することができます。
//  </div>
// </div>


//アニメーション時間の一括設定
var first = 1000;
var medium = 1500;
var slow = 2500;
var ease = 'swing';
//slide2Leftは右から左に、slide2Rightは左から右に行きます。

jQuery(document).ready(function($){
    $('.slide2Left > .anime').css({
        position:'relative',
        left: '100vw',
        height:'100%',
        opacity:'0',
    });

    $('.slide2Left').on('inview', function(event, isInView){
        if(isInView){
            $(this).children('.anime').stop().animate({
                left: '0vw',
                opacity:'1',
            },medium,ease);
        }
    });

    $('.slide2Right > .anime').css({
        position:'relative',
        right: '100vw',
        height:'100%',
        opacity:'0',
    });

    $('.slide2Right').on('inview', function(event, isInView){
        if(isInView){
            $(this).children('.anime').stop().animate({
                right: '0vw',
                opacity:'1',
            },medium,ease);
        }
    });

    $('.slide2Right2 > .anime').css({
        position:'relative',
        right: '100vw',
        height:'100%',
        opacity:'0',
    });

    $('.slide2Right2').on('inview', function(event, isInView){
        if(isInView){
            $(this).children('.anime').stop().animate({
                right: '-25vw',
                opacity:'1',
            },medium,ease);
        }
    });

    $('.slide2Up > .anime').css({
        position:'relative',
        top: '100vw',
        height:'100%',
        opacity:'0',
    });

    $('.slide2Up').on('inview', function(event, isInView){
        if(isInView){
            $(this).children('.anime').stop().animate({
                top: '0vw',
                opacity:'1',
            },medium,ease);
        }
    });



//floatingWordsをクラスに指定すると、文字がしたからふわっと浮かび上がってきます。両方指定する必要あり。
    $('.floatingWordsUp > .anime').css({
        position:'relative',
        bottom: '-50px',
        opacity: '0'
    });


    $('.floatingWordsUp > .anime').on('inview', function(event, isInView){
        if(isInView){
            $(this).stop().animate({
                bottom: '0',
                opacity: '1'
            },medium);
        }
    });


    $('.floatingWordsDown > .anime').css({
        position:'relative',
        top: '-50px',
        opacity: '0'
    });


    $('.floatingWordsDown > .anime').on('inview', function(event, isInView){
        if(isInView){
            $(this).stop().animate({
                top: '0',
                opacity: '1'
            },medium);
        }
    });

    $('.fadeIn > .anime').css({
        opacity: '0'
    });

    $('.fadeIn > .anime').on('inview', function(event, isInView){
        if(isInView){
            $(this).stop().animate({
                opacity: '1'
            },medium);
        }
    });

    $('.fadeInFirst > .anime').css({
        opacity: '0'
    });

    $('.fadeInFirst > .anime').on('inview', function(event, isInView){
        if(isInView){
            $(this).stop().animate({
                opacity: '1'
            },first);
        }
    });

    $('.fadeInSlow > .anime').css({
        opacity: '0'
    });

    $('.fadeInSlow > .anime').on('inview', function(event, isInView){
        if(isInView){
            $(this).stop().animate({
                opacity: '1'
            },slow);
        }
    });

});

