<?php
/********************
 * // 固定ページのみ自動整形機能を無効化します。
 * // 2016年 夏頃追加
 ********************/
function disable_page_wpautop()
{
    if (is_page()) remove_filter('the_content', 'wpautop');
}

add_action('wp', 'disable_page_wpautop');

?>