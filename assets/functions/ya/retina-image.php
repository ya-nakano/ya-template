<?php
/**
 * Created by IntelliJ IDEA.
 * User: nakano
 * Date: 2016/12/05
 * Time: 11:41
 */

function wp_img3x($file, $ext, $alt, $width, $height){
    $url = get_template_directory_uri() . '/assets/images/' . $file;
    echo '<img src="' . $url . $ext . '" srcset="' . $url . '@2x' . $ext . ' 2x,' . $url . '@3x' . $ext . ' 3x" alt="'.$alt.'" width="' . $width . '" height="' . $height . '" />';
}

function img3x($url, $ext, $alt, $width, $height){
    echo '<img src="' . $url . $ext . '" srcset="' . $url . '@2x' . $ext . ' 2x,' . $url . '@3x' . $ext . ' 3x" alt="'.$alt.'" width="' . $width . '" height="' . $height . '" />';
}

function wp_img2x($file, $ext, $alt, $width, $height){
    $url = get_template_directory_uri() . '/assets/images/'  . $file;
    echo '<img src="' . $url . $ext . '" srcset="' . $url . '@2x' . $ext . ' 2x" alt="'.$alt.'" width="' . $width . '" height="' . $height . '" />';
}

function img2x($url, $ext, $alt, $width, $height){
    echo '<img src="' . $url . $ext . '" srcset="' . $url . '@2x' . $ext . ' 2x" alt="'.$alt.'" width="' . $width . '" height="' . $height . '" />';
}

function sc_wp_img3x($atts){
    $url = get_template_directory_uri() . '/assets/images/'  . $atts[0];
    return '<img src="' . $url . $atts[1] . '" srcset="' . $url . '@2x' . $atts[1] . ' 2x,' . $url . '@3x' . $atts[1] . ' 3x" alt="'.$atts[2].'" width="' . $width . '" height="' . $height . '" />';
}

function sc_wp_img2x($atts){
    $url = get_template_directory_uri() . '/assets/images/'  . $atts[0];
    return '<img src="' . $url . $atts[1] . '" srcset="' . $url . '@2x' . $atts[1] . ' 2x" alt="'.$atts[2].'" width="' . $width . '" height="' . $height . '" />';
}

add_shortcode('wp_img3x', 'sc_wp_img3x');
add_shortcode('wp_img2x', 'sc_wp_img2x');

?>