<?php
/********************
 // 管理画面から使えるショートコード
 ********************/

//HOME URL を取得
function shortcode_hurl() {
    return home_url( '/' );
}
add_shortcode('hurl', 'shortcode_hurl');


//PHPパーツを取得
function shortcode_gpart($attr) {
    return get_template_part( $attr[0],$attr[1] );
}
add_shortcode('gpart', 'shortcode_gpart');


//TEMPLATE URL を取得
function shortcode_turl($attr) {
    return get_template_directory_uri('/');
}
add_shortcode('turl', 'shortcode_turl');

?>