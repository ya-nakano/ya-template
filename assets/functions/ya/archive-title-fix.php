<?php
/**
 * Created by IntelliJ IDEA.
 * User: nakano
 * Date: 2017/01/16
 * Time: 17:20
 */

function custom_archive_title( $title ){
    if ( is_post_type_archive() ) {
        if ( is_year() ) {
            $title = post_type_archive_title( '', false ) . ' ' . sprintf( __( 'Year: %s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );
        } elseif ( is_month() ) {
            $title = post_type_archive_title( '', false ) . ' ' . sprintf( __( 'Month: %s' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) );
        } elseif ( is_day() ) {
            $title = post_type_archive_title( '', false ) . ' ' . sprintf( __( 'Day: %s' ), get_the_date( _x( 'F j, Y', 'daily archives date format' ) ) );
        } else {
            $title = post_type_archive_title( '', false );
        }
    } else if ( is_tax() ) {
        $title = single_term_title( '', false );
    } elseif ( is_year() ) {
        $title = 'ブログ ' . sprintf( __( 'Year: %s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );
    } elseif ( is_month() ) {
        $title = 'ブログ ' . sprintf( __( 'Month: %s' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) );
    } elseif ( is_day() ) {
        $title = 'ブログ ' . sprintf( __( 'Day: %s' ), get_the_date( _x( 'F j, Y', 'daily archives date format' ) ) );
    }
    return $title;
}
add_filter( 'get_the_archive_title', 'custom_archive_title', 10 );