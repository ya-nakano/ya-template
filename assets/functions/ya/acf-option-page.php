<?php
/* ACF Option Pageを追加します。 */
/**
 * Created by IntelliJ IDEA.
 * User: nakano
 * Date: 2016/12/05
 * Time: 11:20
 */

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'サイトの全体設定',
        'menu_title' => '全体設定',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'サイトヘッダーの設定',
        'menu_title' => 'ヘッダー',
        'menu_slug' => 'theme-header-settings',
        'parent_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'サイトフッターの設定',
        'menu_title' => 'フッター',
        'menu_slug' => 'theme-footer-settings',
        'parent_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
    ));
}

?>